# Basic React App

This react app enables the user to add, edit and delete notes (CRUD)

## Project Setup

this project has been  tested on node version 8.4 i recommend you either use the same node version.

Download the git repo or clone

in CLI view inside of the repo folder and follow these instructions:

- to install npm packages `npm i`
- to run application. `npm start`

this application uses react scripts library to allow us to build proof of concepts. for inital development we have only created development enviroment. we will eventually add in the production build and move the build to use the latest version of webpack to enable us to seperate out the css from the js and also improve control over the output of js code and improving size and speed on the website. 

This app was built within 1 evening using common libraries and previous experience using CRUD scripts. 

I started this app at 8:30pm and currently writing this README at 2am. 


### Issues / Bugs 

- when you click on a list item and click view sometimes it might show the previous selected note. when you refresh the browser it will show the correct note.
- Notes need to store and maintain session data rather than just state..


