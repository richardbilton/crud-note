export default {
    notesReducer: {
        notes: []
    },

    selectedNoteReducer: {
        note: undefined
    },

    apiReducer: {
        apiCallsInProgress: 0
    }
};