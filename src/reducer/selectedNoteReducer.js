import * as ActionType from '../action/ActionType';
import initState from './initState';
import _ from 'lodash';


const selectedNoteReducer = (state = initState.selectedNoteReducer, action) => {
    switch(action.type) {

        case ActionType.GET_NOTE_RESPONSE: {
            return {
                ...state,
                note: _.assign(action.note)
            };
        }


        default: { return state; }
    }
};


export default selectedNoteReducer;