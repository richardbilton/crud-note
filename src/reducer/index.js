import {combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';
import notesReducer from './notesReducer';
import selectedNoteReducer from './selectedNoteReducer';
import apiReducer from './apiReducer';

export default combineReducers({
    /* Reducers will be included here */   
    notesReducer,
    selectedNoteReducer,
    apiReducer,
    form: formReducer 
});