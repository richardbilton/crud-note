import initState from './initState';
import _ from 'lodash';
import * as ActionType from '../action/ActionType';

const notesReducer = (state = initState.notesReducer, action) => {
    switch(action.type) {
        case ActionType.GET_NOTES_RESPONSE: {

            return {
                ...state, 
                notes: _.assign(action.notes)
            };
        }


        default: { return state; }
    }
};



export default notesReducer;