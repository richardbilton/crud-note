const notes = [
    {
        id: "1",
        title: "St. Jerome",
        content: "Good, better, best. Never let it rest. 'Til your good is better and your better is best."
    },
    {
        id: "2",
        title: "Swam Sivananda",
        content: "Put your heard, mind and soul into even your smallest acts. This is the secret to success."
    },
    {
        id: "3",
        title: "Buddha",
        content: "Do no dwell in the past, do not dream of the future, concentrate the mind on the present moment."
    },
    {
        id: "4",
        title: "Thomas A. Edison",
        content: "Our greatest weakness lies in giving up. The most certain way to succeed is always to try just one more time."
    },
];

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}

//This would be performed on the server in a real app. Just stubbing in.
const generateId = (note) => {
    return replaceAll(note.title, ' ', '-');
};

class NoteApi {
    static getAllNotes() {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(Object.assign([], notes));
            }, 300);
        });
    }

    static saveNote(note) {
        note = Object.assign({}, note); // to avoid manipulating object passed in.
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                // Simulate server-side validation
                const minNoteTitleLength = 1;
                if (note.title.length < minNoteTitleLength) {
                    reject(`Title must be at least ${minNoteTitleLength} characters.`);
                }

                if (note.id) {
                    const existingNoteIndex = notes.findIndex(a => a.id === note.id);
                    notes.splice(existingNoteIndex, 1, note);
                } else {
                    note.id = generateId(note);
                    note.watchHref = `http://www.google.com${note.id}`;
                    notes.push(note);
                }

                resolve(note);
            }, 300);
        });
    }

    static deleteNote(noteId) {
        return new Promise((resolve) => {
            setTimeout(() => {
                const indexOfNoteToDelete = notes.findIndex(note => note.id === noteId);
                notes.splice(indexOfNoteToDelete, 1);
                resolve();
            }, 300);
        });
    }


    static getNote(noteId) {
        return new Promise((resolve) => {
            setTimeout(() => {
                const existingNoteIndex = notes.findIndex(note => note.id === noteId);
                
                const noteFound = Object.assign({}, notes[existingNoteIndex]);

                resolve(noteFound);

            }, 300);
        });
    }

}

export default NoteApi;