import React from 'react';
import {render} from 'react-dom';
import configStore from './configStore';
import {Provider} from 'react-redux';

import App from './components/App';


/* bootstrap */
import 'bootstrap/dist/js/bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

/* custom style */
import './style/style.css';

const store = configStore();

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('app')
);