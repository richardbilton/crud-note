import * as ActionType from './ActionType';
import NoteApi from '../Api/NoteApi';
import { ApiCallBeginAction, ApiCallErrorAction } from './ApiAction';


export const getNotesResponse = notes => ({
    type: ActionType.GET_NOTES_RESPONSE,
    notes
});

export function getNotesAction() {
    return (dispatch) => {

        dispatch(ApiCallBeginAction());

        return NoteApi.getAllNotes()
            .then(notes => {
                dispatch(getNotesResponse(notes));
            }).catch(error => {
                throw error;
            });
    };
}

export const addNewNoteResponse = () => ({
    type: ActionType.ADD_NEW_NOTE_RESPONSE
});

export const updateExistingNoteResponse = () => ({
    type: ActionType.UPDATE_EXISTING_NOTE_RESPONSE
});

export function saveNoteAction(noteBeingAddedOrEdited) {
    return function (dispatch) {

        dispatch(ApiCallBeginAction());

        return NoteApi.saveNote(noteBeingAddedOrEdited)
            .then(() => {
                if (noteBeingAddedOrEdited.id) {
                    dispatch(updateExistingNoteResponse());
                } else {
                    dispatch(addNewNoteResponse());
                }
            }).then(() => {
                dispatch(getNotesAction());
            }).catch(error => {
                dispatch(ApiCallErrorAction());
                throw (error);
            });
    };
}

export const getNoteResponse = noteFound => ({
    type: ActionType.GET_NOTE_RESPONSE,
    note: noteFound
});

export function getNoteAction(noteId) {
    return (dispatch) => {

        dispatch(ApiCallBeginAction());

        return NoteApi.getNote(noteId)
            .then(note => {
                dispatch(getNoteResponse(note));
            }).catch(error => {
                throw error;
            });
    };
}

export const deleteNoteResponse = () => ({
    type: ActionType.DELETE_NOTE_RESPONSE
});

export function deleteNoteAction(noteId) {
    return (dispatch) => {

        dispatch(ApiCallBeginAction());

        return NoteApi.deleteNote(noteId)
            .then(() => {
                dispatch(deleteNoteResponse());
            }).then(() => {
                dispatch(getNotesAction());
            }).catch(error => {
                throw error;
            });
    };
}