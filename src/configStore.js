import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducer';



const configStore = initState => {
    return createStore(
        rootReducer, 
        initState,
        applyMiddleware(thunk)
    );
};



export default configStore;