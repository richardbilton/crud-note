import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import toastr from 'toastr';
import * as noteAction from '../../action/NoteAction';
import NoteForm from './NoteForm'; 



export class AddOrEditNoteContainer extends React.Component {


    constructor() {
        super();
        this.handleSave = this.handleSave.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }



    componentDidMount() {
        this.props.action.getNoteAction(this.props.match.params.id)
            .catch(error => {
                toastr.error(error);
            });
    }



    handleSave(values) {
        const note = {
            id: values.id,
            title: values.title,
            watchHref: values.watchHref,
            content: values.content
        };

        this.props.action.saveNoteAction(note)
            .then(() => {
                toastr.success('Note Added!');
                this.props.history.push('/');
            }).catch(error => {
                toastr.error(error);
            });
    }



    handleCancel(event) {
        event.preventDefault();
        this.props.history.replace('/');
    }



    render() {
        const { initialValues } = this.props;

        const heading = initialValues && initialValues.id ? 'Edit' : 'Add';

        return (
            <div className="container">
                <NoteForm
                    heading={heading}
                    handleSave={this.handleSave}
                    handleCancel={this.handleCancel}
                    initialValues={this.props.initialValues}
                />
            </div>
        );
    }
}



const mapStateToProps = (state, ownProps) => { 

    return {
            initialValues: state.selectedNoteReducer.note,
        };
};



const mapDispatchToProps = dispatch => ({
    action: bindActionCreators({ ...noteAction }, dispatch)
});



AddOrEditNoteContainer.propTypes = {
    action: PropTypes.object.isRequired,
    history: PropTypes.object,
    initialValues: PropTypes.object,
    match: PropTypes.object.isRequired
};



export default connect(mapStateToProps, mapDispatchToProps)(AddOrEditNoteContainer);