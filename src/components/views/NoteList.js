import React, { PropTypes } from 'react';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';



class NoteList extends React.Component {

    constructor(props) {
        super(props);

        this.selectRowProp = {
            mode: 'radio',
            bgColor: '#85a6bc',
            onSelect: props.handleRowSelect,
            clickToSelect: true, 
            hideSelectColumn: true            
        };
    }

    render() {

        return (
            
            <BootstrapTable data={this.props.notes}  selectRow={this.selectRowProp} hover condensed>
                <TableHeaderColumn dataField="id" isKey hidden>Id</TableHeaderColumn>
                <TableHeaderColumn 
                    dataField="title"
                    columnTitle
                >
                </TableHeaderColumn>

            </BootstrapTable>
        );

    }
}

NoteList.propTypes = {
    notes: PropTypes.array.isRequired,
    handleRowSelect: PropTypes.func.isRequired
};

export default NoteList;