import React, { PropTypes } from 'react';
import List from './NoteList';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as noteAction from '../../action/NoteAction';

import toastr from 'toastr';
/* I've only used toastr for POC purposes, it requires jQuery which is not ideal for production. may change. */

/* Header */
import Header from './Header';

export class Home extends React.Component {

    constructor() {
        super();

        this.state = {selectedNoteId: undefined};

        this.handleAddNote = this.handleAddNote.bind(this);
        this.handleEditNote = this.handleEditNote.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleRowSelect = this.handleRowSelect.bind(this);
    }

    componentDidMount() {
        this.props.action.getNotesAction()
            .catch(error => {
                toastr.error(error);
            });
    }

    handleAddNote() {
        /* go to add */
        this.props.history.push('/note');
    }

    handleEditNote() {
        /* store state in constant */
        const selectedNoteId = this.state.selectedNoteId;

        if (selectedNoteId) {
            /* add ID to state */
            this.setState({selectedNoteId: undefined}); 
            
            /* go to edit */
            this.props.history.push(`/note/${selectedNoteId}`);
        }        
    }

    handleDelete() {
        /* store state into constant */
        const selectedNoteId = this.state.selectedNoteId;

        if (selectedNoteId) {
            /* remove id from selected state */
            this.setState({selectedNoteId: undefined}); 
            
            /* run delete action */
            this.props.action.deleteNoteAction(selectedNoteId)
                .catch(error => {
                    toastr.error(error);
                });
        }
    }

    handleRowSelect(row, isSelected) {
        if (isSelected) {
            /* pass selected note id into state */
            this.setState({selectedNoteId: row.id});
        }
    }

    render() {

        const { notes } = this.props;

        if (!notes) {
            return (
                <div>Start by adding a note. Click "Add" Button</div>
            );
        }

        return (
            <div>

                <Header />

                <div className="container">

                    <div className="row">
                        <div className="col">
                            <List notes={notes} handleRowSelect={this.handleRowSelect}/>
                        </div>
                    </div>

                    <div className="btn-group btn-group-lg" role="group">
                        <button
                            type="button"
                            className="btn btn-primary"
                            onClick={this.handleAddNote}
                        >
                        New
                        </button>

                        <button
                            type="button"
                            className="btn btn-warning"
                            onClick={this.handleEditNote}                                
                        >
                        View / Edit
                        </button>  

                        <button
                            type="button"
                            className="btn btn-danger"
                            onClick={this.handleDelete}
                        >
                        Delete
                        </button>   
                    </div>           

                </div>
                
            </div>
        );
    }
};



const mapStateToProps = state => ({
    notes: state.notesReducer.notes
});



const mapDispatchToProps = dispatch => ({
    action: bindActionCreators(noteAction, dispatch)

});



Home.propTypes = {
    notes: PropTypes.array,
    action: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
};



export default connect(mapStateToProps, mapDispatchToProps)(Home);