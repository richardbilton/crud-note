import React from 'react';



export class Header extends React.Component {

    render() {
        return (
            <header>
                <div className="navbar navbar-light bg-light">
                    <div className="container">
                        <a href="/" className="navbar-brand">Notes</a>
                    </div>
                </div>
            </header>
        );
    }

}

export default Header;
