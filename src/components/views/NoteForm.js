import React, { PropTypes } from 'react';
import { Field, reduxForm } from 'redux-form';
import FieldInput from '../common/FieldInput';
import Header  from './Header';


export const NoteForm = ({ handleSubmit, pristine, reset, submitting, heading, handleSave, handleCancel }) => {
    return (

        <div>

        <Header />

        <div className="container">

            <form onSubmit={handleSubmit(handleSave)}>

                <Field
                    type="text"
                    name="title"
                    placeholder="Note Title"
                    component={FieldInput}
                />

                <Field
                    type="textarea"
                    name="content"
                    label="Content"
                    placeholder="Write your note message here..."
                    component="textarea"
                    className="form-control"
                    rows="10"
                />

                <div className="form-btn-group">
                    <button type="submit" disabled={submitting} className="btn btn-primary btn-lg">Save</button>

                    {heading === 'Add' && <button type="button" disabled={pristine || submitting} onClick={reset} className="btn btn-default btn-lg">Start Over</button>}

                    <button type="button" className="btn btn-default pull-right btn-lg" onClick={handleCancel}>Back</button>
                </div>
            </form>

        </div>

        </div>
    );
};





const validate = values => {
    const errors = {};

    if (!values.title) {
        errors.title = 'Required';
    }

    return errors;
};



NoteForm.propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    pristine: PropTypes.bool.isRequired,
    reset: PropTypes.func.isRequired,
    submitting: PropTypes.bool.isRequired,
    handleSave: PropTypes.func.isRequired,
    handleCancel: PropTypes.func.isRequired
};



export default reduxForm({
    form: 'NoteForm',
    validate
})(NoteForm);