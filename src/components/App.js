import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';
import AddOrEditNoteContainer from './views/AddOrEditNoteContainer';

/* Components */

import Home from './views/Home';

const history = createBrowserHistory();

const App = () => {
    return (
        <div >
            <Router history={history}>
                <div>

                    <Switch>
                        <Route exact path="/" component={Home} />
                        <Route exact path="/note" component={AddOrEditNoteContainer} />
                        <Route path="/note/:id" component={AddOrEditNoteContainer} />
                    </Switch>

                </div>

            </Router>
        </div>
    );
};


export default App;